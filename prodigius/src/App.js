import React from 'react';
import GlobalStyle from './styles/global'

import Navbar from './components/menu/Navbar/Navbar';
import Home from './pages/home/index'

const App = () => (
  <>
    <GlobalStyle />

    <div>
      <Navbar />
      <Home />
    </div>


  </>


)

export default App;
