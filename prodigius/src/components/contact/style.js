import styled from 'styled-components'


export const Container = styled.div`
width: 100%;
height:100;
display:flex;
flex-direction: column;
background: white;
margin-top:20px;
/* @font-face {
        font-family: 'gotham';
        src: local('gotham'), url('../fonts/Gotham-Bold.otf') format('otf');
        font-style: normal;
        font-weight: bold;
  } */

`;

export const Title = styled.p`
font-size: 1.7rem;
/* font-weight:900; */
font-weight:600;
span{
    font-weight:50;
}
`;

export const Modaltitle  = styled.p`
font-size:1.5rem;
text-align:center;
padding:20% 10% 5% 10%;
`;
export const Modalgreen = styled.p`
color: #8ec642;
font-size:1.7rem;
text-align:center;
padding:0% 5% 25% 5%;
font-weight:600;

`;
export const Modalbutton = styled.button`
font-family: 'Raleway', sans-serif !important;
color: #8ec642;
border: none;
font-weight:900;
font-size:1.9rem;
margin-left:90%;
cursor: pointer;
`;



export const Error = styled.p`
font-size: 1rem;
color:#e90000 !important;
font-weight:300 !important;
`;

export const Inputs = styled.div`
    display: flex;
    flex-direction:column;

`;
export const Input = styled.div`
 max-width:600px;
 margin-top:15px;
 .active{
        border:1px solid #e90000 !important;
    }
    .component{
        border:1px solid #4d2682 !important;
    }
p{
    color:#4d2682;
    margin-bottom:5px;
    font-weight:700;
}
input{
    width:100%;
    /* border:1px solid #4d2682; */
    border-radius:3px;
    color:#4d2682;
    height:30px;
    padding: 5px;
    

}
textarea{
    width:100%;
    border:1px solid #4d2682;
    border-radius:3px;
    color:#4d2682;
    resize: none;
    height:120px;
    padding: 5px;
}

`;
export const Button = styled.div`
    background:#4d2682;
    color:white;
    font-weight:600;
    display:flex;
    align-items:center;
   justify-content:center;
   max-width:300px;
    border-radius:3px;
    margin:30px 0;
    height:50px;
`;
export const CustomStyles = {
    content: {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    },
    overlay: {
  
    },
};