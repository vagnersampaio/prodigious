import React, { useState } from 'react';
import { Container, Title, Inputs, Input, Button, Error, CustomStyles, Modaltitle, Modalgreen, Modalbutton } from "./style"
import MaskedInput from 'react-text-mask'
import Modal1 from 'react-modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const Home = () => {
    const [contact, setcontact] = useState({})
    const [flag, setflag] = useState(false)
    const [modalIsOpenc, setIsOpenc] = useState(false);
    function send() {
        setflag(true)
        //valida se todos os campos estão preenchidos
        if (contact.name && contact.phone && contact.Email && contact.message && (contact.Email.indexOf("@") != -1) && contact.phone.indexOf("_") == -1) {
            openModal()

        }

    }

    function openModal() {
        setIsOpenc(true);
    }

    function closeModal() {
        setIsOpenc(false);
    }


    return (
        <>
            <Modal1
                isOpen={modalIsOpenc}

                onRequestClose={closeModal}
                style={CustomStyles}
            >
                <Modalbutton onClick={closeModal}><FontAwesomeIcon icon={faTimes} /></Modalbutton>
                <Modaltitle >Thank you for getting in touch.</Modaltitle>
                <Modalgreen>Have a great day!</Modalgreen>

            </Modal1>

            <Container>
                <Title><span>\</span>CONTACT <span>US</span></Title>
                <Inputs>
                    <Input>
                        <div>
                            <p>Name:</p>
                            <input onChange={e => { setcontact({ ...contact, ["name"]: e.target.value }) }} className={(flag && !contact.name) ? "active" : "component"}  ></input>
                            {flag && !contact.name && <>
                                <Error>(fill this field)</Error>
                            </>
                            }

                        </div>

                    </Input>
                    <Input>
                        <div>
                            <p>Phone:</p>

                            <MaskedInput className={(flag && !contact.phone) ? "active" : "component"} mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]} onChange={e => { setcontact({ ...contact, ["phone"]: e.target.value }) }} />
                            {flag && !contact.phone && <>
                                <Error>(fill this field)</Error>
                            </>
                            }
                            {/* valida se fone está completo com todos os dígitos  */}
                            {flag && contact.phone && <>
                                {
                                    (contact.phone.indexOf("_") != -1) && <>
                                        <Error>(Phone invalid)</Error>
                                    </>
                                }
                            </>
                            }
                        </div>

                    </Input>
                    <Input>
                        <div>
                            <p>E-mail:</p>
                            <input className={(flag && !contact.Email) ? "active" : "component"} onChange={e => { setcontact({ ...contact, ["Email"]: e.target.value }) }} ></input>
                            {flag && !contact.Email && <>
                                <Error>(fill this field)</Error>
                            </>
                            }
                            {/* valida se o email possui um @ */}
                            {flag && contact.Email && <>
                                {
                                    (contact.Email.indexOf("@") == -1) && <>
                                        <Error>(Email invalid)</Error>
                                    </>
                                }
                            </>
                            }

                        </div>

                    </Input>
                    <Input>
                        <div>
                            <p>Message:</p>
                            <textarea className={(flag && !contact.message) ? "active" : "component"} onChange={e => { setcontact({ ...contact, ["message"]: e.target.value }) }} ></textarea>
                            {flag && !contact.message && <>
                                <Error>(fill this field)</Error>
                            </>
                            }
                        </div>

                    </Input>

                </Inputs>

                <Button onClick={send}>Send</Button>
            </Container>

        </>
    )
}

export default Home