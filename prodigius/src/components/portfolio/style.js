import styled from 'styled-components'


export const Container = styled.div`


background: black;
margin-bottom:20px;
margin-top:30px;
color: white;

`;
export const Containerexterno = styled.div`
width:100%;
display:flex;
justify-content:center;

`;

export const Box = styled.div`
    display:flex;
    background: #11b6e0;
    justify-content: space-between;
    align-items:center;
    padding: 10px 30px;
    border:none;
    cursor: pointer;
    width:100%;
    svg {
      fill: white;
      align-items:top;
    }
 

`;
export const Title = styled.p`
font-size: 1.7rem;
/* font-weight:900; */
font-weight:600;
margin-top:45px;
margin-left:30px;
span{
    font-weight:50;
}

`;


export const Select = styled.div`
    display:flex;
    background: #11b6e0;
    flex-direction:column;
    padding: 0 30px;
    cursor: pointer;
    width:100%;
    p{
        padding:5px 0;
    }
   
`;
export const List  = styled.div`
color: black;
@media (min-width: 481px) {
    max-width:481px;
    width:100%;
    display:flex;
    flex-wrap: wrap;
    justify-content:center;
   }
   @media (min-width: 769px) {
    max-width:769px;
    width:100%;
    display:flex;
    flex-wrap: wrap;
    justify-content:center;
   }
   @media (min-width: 1201px) {
    max-width:1201px;
   }
   @media (min-width: 1213px) {
    max-width:100%;
    
   }

`;
export const Button = styled.div`
    width:100%;
   color: white;
   margin: 30px;
   text-align: center;
   color: #11b6e0;
    border: 1px solid #11b6e0;
    border-radius: 4px;
    font-size: 14px;
    font-weight: 600;
    padding: 10px 0;
    @media (min-width:   769px) {
    max-width:240px;
   }
`;