import React, { useState, useEffect } from 'react';
import { Container, Box, Select, List, Button, Title, Containerexterno } from "./style"
import Selectcomponet from '../selectcomponet/index'
import Lista from '../card/index'
import Card from '../card/index'
import { ReactComponent as ReactLogo } from '../../assets/svg/arrow.svg'
import Renault from "../../assets/photo1.png"
import Renaultsport from '../../assets/photo2.png'
import Cadillac from '../../assets/photo5.png'
import mcdonalds from '../../assets/photo4.png'
import nescafe from '../../assets/photo3.png'
import gmc from '../../assets/GMC.jpg'
import bountypng from '../../assets/bountypng.jpg'
import frontline from '../../assets/frontline.jpg'
import canoneosm6 from '../../assets/canoneosm6.jpg'
import Collapse from '@material-ui/core/Collapse';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const data = [
  { name: "Renault", img: Renault, cat: "Automotive", type: "MOVINGIMAGE" },
  { name: "McDonald's", img: mcdonalds, cat: "retail", type: "SOUND" },
  { name: "Canon", img: canoneosm6, cat: "Fast Moving Consumer Goods", type: "STILLIMAGE" },
  { name: "Sanofi, Frontline", img: frontline, cat: "Fast Moving Consumer Goods", type: "DIGITAL" },
  { name: "Renaultsport", img: Renaultsport, cat: "Automotive", type: "ADAPTATION&LOCALIZATION" },
  { name: "Carrefour", img: "https://prodigious.com/portfolio/img/projects/carrefouroptimisme/carrefouroptimisme.jpg", cat: "retail", type: "MOVINGIMAGE" },
  { name: "Nescafé", img: nescafe, cat: "Fast Moving Consumer Goods", type: "SOUND" },
  { name: "Cartier", img: "https://www.prodigious.com/portfolio/img/projects/cartierchristmas2017/cartierchristmas2017.jpg", cat: "Luxury/beauty/fashionry", type: "STILLIMAGE" },
  { name: "Cadillac", img: Cadillac, cat: "Automotive", type: "DIGITAL" },
  { name: "GMC", img: gmc, cat: "Automotive", type: "ADAPTATION&LOCALIZATION" },
  { name: "Bounty P&G", img: bountypng, cat: "retail", type: "MOVINGIMAGE" }]

const options = [{
  name: "All",
  cat: "All",


}, {
  name: "Automotive",
  cat: "Automotive",

},
{
  name: "Retail",
  cat: "retail",

}, {
  name: "Fast Moving Consumer Goods",
  cat: "Fast Moving Consumer Goods",

}, {
  name: "Luxury/beauty/fashion",
  cat: "Luxury/beauty/fashionry",

}]


const Portifolio = () => {
  const [portifolios, setportifolios] = useState([{}])
  const [open, setopen] = useState(false)
  const [cont, setcont] = useState(6)
  const [selecionado, setselecionado] = useState('All')


  useEffect(() => {
    if (selecionado == 'All') {
      setportifolios(data.slice(0, 6))
    } else {
      handlesearch()
    }
    setcont(6)
  }, [selecionado]);

  function handlesearch() {
    setportifolios(data.filter(fucfilter).slice(0, 6))
  }

  function fucfilter(value) {
    return value.cat == selecionado;
  }
  function loremore() {
    if (selecionado == 'All') {
      setportifolios(data.slice(0, (cont + 6)))
    } else {
      setportifolios(data.filter(fucfilter).slice(0, (cont + 6)))
    }
    setcont(cont + 6)
  }

  function handleclick() {
    setopen(!open)
  }
  function handleselect(option) {
    setselecionado(option)
    setopen(!open)

  }
  
  return (
    <>
      <Title><span>\</span> PORTFOLIO</Title>
      <Container>

        <Box onClick={handleclick}>
          <div>
            {selecionado}
          </div>
       
          {open ? <ExpandMoreIcon ></ExpandMoreIcon> : <ExpandLessIcon></ExpandLessIcon>}
      
          
        </Box>

        <Collapse in={open} Teste>
          <div elevation={4}>
            <Select>
              {options.map(option => (
                <div onClick={() => { handleselect(option.cat) }}>
                  <p>{option.name}</p>
                </div>
              ))}
            </Select>
          </div>
        </Collapse>


        <Containerexterno>
          <List>
            {portifolios.map(portifolio => (
              <Card data={portifolio} />
            ))}

          </List>
        </Containerexterno>
        <Containerexterno>
        <Button onClick={() => { loremore() }}>load more</Button>
        </Containerexterno>
      </Container>

    </>
  )
}

export default Portifolio