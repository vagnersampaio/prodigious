import React from 'react';
import { Container, Textaling, Text } from "./style"
import Carousel from '../carousel/index'

 const Home = () => {

    return(
    <>
    <Container>
        
        <Carousel />
        <Textaling className="teste">
            <Text>
            We design, produce and deliver <span>brand content</span> across <span>all chanel</span>, using the best talent, processes and tools.
             The result? Seamless global execution of multiple content types across <span>all markets</span>, without compromising <span>creative quality</span>.
            </Text>
        </Textaling>
  
        
         </Container>
    
    </>
    )
}

export default Home