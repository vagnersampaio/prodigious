import styled from 'styled-components'


export const Container = styled.div`
width: 100%;
height:100;
display:flex;
flex-direction: column;
background: white;
justify-content: space-between;
align-items:center;
margin-top:100px;


@media (min-width: 768px) {
    flex-direction: row;
  }

`;
export const Textaling= styled.div`

max-width:600px;


`;
export const Text= styled.p`
color: black;
font-size:1.3rem;
line-height:1.4;
span{
  color: #ee303d;
font-size:1.3rem;
}
@media (min-width: 481px) {
  font-size:1.1rem;
   }
@media (min-width: 769px) {
     margin-left: 10px;
    font-size:1rem;
    span{
       
        font-size:0.9rem;
      }
   }
@media (min-width: 1201px) {
  font-size:1.3rem;
    span{
       
        font-size:1.2rem;
      }
   }
@media (min-width: 1213px) {
  font-size:1.4rem;
    span{
       
        font-size:1.3rem;
      }
    
   }
`;
