import styled from 'styled-components'


export const Container = styled.div`

display:flex;
background: white;
margin-bottom:20px;
padding: 30px 0px 0 0px;
@media (min-width: 769px) {
    width:1200px;
   }
`;
export const VideoPosition = styled.div`
width: 100%;
height:100%;
display:flex;
background: white;
justify-content: center;

video{
    width: 100%;
    height:100;

}

@media (max-width: 768px) {
    .MuiIconButton-root{
        
        display:none;
       
        
    }
  }

`;
