import React from 'react';
import { Container, VideoPosition } from "./style"
import Carousel from 'react-material-ui-carousel'
import video from '../../assets/video/video.mp4'


const Home = () => {

  return (
    <>
      <Container>
        <VideoPosition>
          <Carousel autoPlay={false}>
            <video controls src={video} ></video>
            <video controls src={video} ></video>
          </Carousel>
        </VideoPosition>


      </Container>

    </>
  )
}

export default Home