import React from 'react';
import { Container, Img, Plus, Box1, Adaptation, Digital, Still, Sound, Moving } from "./style"
import { ReactComponent as ReactLogo } from '../../assets/svg/arrow.svg'


const Card = ({ data }) => {
  function color() {

    switch (data.type) {
        case 'MOVINGIMAGE':
            return <Adaptation><div><Plus>+</Plus></div></Adaptation>

        case 'SOUND':
            return <Digital><div><Plus>+</Plus></div></Digital>
        case 'STILLIMAGE':
            return <Still><div><Plus>+</Plus></div></Still>

        case 'DIGITAL':
            return <Sound><div><Plus>+</Plus></div></Sound>
        case 'ADAPTATION&LOCALIZATION':
            return <Moving><div><Plus>+</Plus></div></Moving>
    }
}

  return (
    <Container>
      <Img src={data.img}></Img>
      <Box1>
        <div><p>{data.name}</p></div>
        {color()}
      </Box1>
    </Container>


  )
}

export default Card