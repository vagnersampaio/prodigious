import styled from 'styled-components'


export const Container = styled.div`
    width:100%;
    max-width:320px;
    background:black;
    color:white;
    display: flex;
    flex-direction: column;
    @media (min-width: 481px) {
    max-width:240px;
   }
   /* @media (min-width:   1201px) {
    max-width:240px;
   } */
   @media (min-width: 1201px) {
    max-width:400px;
   }
   @media (min-width: 1213px) {
    max-width:33%;
    height:300px;
   }
  
`;

export const Box1 = styled.div`
   display: flex;
   flex-direction: row !important;
   justify-content: space-between;
   align-items:center;
   padding:0px 30px 5px 30px;
   div{
    p{align-items:center}
   }
   
    
`;
export const Img = styled.img`
   /* max-width:320px; */
   max-width:100%;
   width:100%;
   height:194px;
   @media (min-width: 481px) {
    /* max-width:240px; */
    height:150px;
   }
   @media (min-width: 1201px) {
    /* max-width:400px; */
    height:200px;
   }
   @media (min-width: 1213px) {
    
    height:250px;
   }
   
`;
export const Plus = styled.p`
  font-weight:600;
    text-align: right;
   font-size:35px;
`;

// 320 × 194
export const Moving = styled.span`
  color: #cc3399;
`;
export const Sound = styled.span`
  color: #11b6e0;
`;
export const Still = styled.span`
  color: #ef4323;
`;
export const Digital = styled.span`
  color: #8ec642;
`;
export const Adaptation = styled.span`
  color: #f48318;
`;