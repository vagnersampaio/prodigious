import React from 'react';
import { Nav } from "./styles";
import Burger from '../Burger/index';
import { ReactComponent as ReactLogo } from '../../../assets/svg/logo_prodigious.svg'

const Navbar = () => {
  return (
    <Nav>
      <ReactLogo />
      <Burger />
    </Nav>
  )
}

export default Navbar