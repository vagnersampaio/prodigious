import styled from 'styled-components';

export const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  li {
    padding: 8px 10px;
    font-size:1.2rem;
    a {
      text-decoration:none !important;
    }
  }
  @media (max-width: 768px) {
    flex-flow: column nowrap;
    background-color: #000000;
    position: fixed;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 100%;
    z-index:10;
    padding-top: 8rem;
    transition: transform 0.3s ease-in-out;
    a {
      color: #fff !important;
      text-decoration:none !important;
    }
  }
`;