import styled from 'styled-components'


export const Container = styled.div`
    display:flex;
    background: white;
    margin-bottom:20px;
    width:100%;
`;

export const Box = styled.div`
    display:flex;
    background: #11b6e0;
    justify-content: space-between;
    align-items: center;
    padding: 0 30px;
    cursor: pointer;
    width:100%;
    svg {
      fill: white;
    }
`;
