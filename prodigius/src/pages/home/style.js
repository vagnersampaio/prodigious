import styled from 'styled-components'


export const Container = styled.div`
width: 100%;
height:100;
display:flex;
flex-direction:column;
background: white;


`;
export const Space = styled.div`
padding: 0 30px;
`;

export const Button = styled.button`
margin-top:7px;
padding: 0 30px;
`;

export const CustomStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        backgroundColor: '#000000',
        fontSize: '15px',
        width: "100%",
        marginRight: '-50%',
        paddingBottom: "40px",
        transform: 'translate(-50%, -50%)'
    },
    overlay: {
        position: 'fixed',
        top: "95%",
        width: "100%",
        left: 0,
        right: 0,
        bottom: 10,
        backgroundColor: '#000000'
    },
};