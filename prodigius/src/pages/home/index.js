import React, {useEffect, useState} from 'react';
import { Container, Space, Button, CustomStyles } from "./style"
import Section1 from '../../components/home/index'
import Contact from '../../components/contact/index'
import Portfolio from '../../components/portfolio/index'
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { useCookies } from 'react-cookie';


const Home = () => {
    const [cookies, setCookie] = useCookies(['Alright']);
    useEffect(() => {
        if(!cookies.Alright){
            openModal()
        }
        
       
      }, []);
      
    let subtitle;

    const [modalIsOpen, setIsOpen] = useState(false);
    function openModal() {
        setIsOpen(true);
    }

    function afterOpenModal() {
        // references are now sync'd and can be accessed.
        subtitle.style.color = '#ffffff';
    }

    function closeModal() {
        setIsOpen(false);
        setCookie('Alright', true)
    }
    return (
        <>
            <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                style={CustomStyles}
            >

                <p ref={_subtitle => (subtitle = _subtitle)}>This website uses cookies: read more</p>
                <Button onClick={closeModal}>Alright</Button>

            </Modal>


            <Container>

                <Space>
                    <Section1 />
                </Space>
                <Portfolio />
                <Space>
                    
                    <Contact  />
                    
                    
                </Space>
            </Container>
           

        </>
    )
}

export default Home

